﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_Project_Enterprise
{
    class ProductRepository
    {
        public class ProductRepository : Repository<Product>
        {
            public override Product Model { get; set; }

            public overide Product Detail(dynamic result)
            {
                var entity = new Product()
                {
                    Id = result["Id"].ToString() as string,
                    Description = result["Description"].ToString() as string,
                };
                return entity;
            }

            public override string Query(product entity = null)
            {
                var query = string.Empty;

                switch (verb)
                {
                    case DataVerb.Get:
                        query = "";
                        break;
                    case DataVerb.Post:
                        query = string.Format("", enity.Id, enity.Description);
                        break;
                    case DataVerb.Put:
                        query = string.Format("", enity.Id, enity.Description);
                        break;
                    case DataVerb.Delete:
                        query = string.Format("", enity.Id);
                        break;
                    case DataVerb.Generate:
                        query = "";
                        break;
                }
                return query;
            }
        }
    }
}
